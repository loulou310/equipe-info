# Serveur Minecraft

Bienvenue ici ! 

Si vous êtes la, c'est que vous reprenez le serveur Minecraft, ou vous êtes membre de l'équipe info, le jour de la fête de SFX et que vous avez oublié comment ça marche. Je vais ici expliquer le fontionnement de base du serveur

## Lancer le serveur

Si vous êtes sur le PC serveur (Et je vous le souhaite), lancer le ficher `start.sh` vous permettera de lancer le serveur sans encombre. Si vous savez vraiment pas comment faire, Clic droit dans le dossier `minecraft-server` -> Ouvrir un terminal ici -> `./start.sh`.

Dans le cas ou vous êtes ni sur le PC serveur, ni sous Windows, eh bah exécutez dans un terminal `java -Xms1g -Xmx8g -jar Magma.jar`

## Mettre a jour le serveur

Le serveur utilise Git comme système de controle de version, et framagit pour le stockage en ligne. Si vous avez fait des modifications, `git pull` pour récupérer les changements. Pareil si vous avez fait des modifications en equipe, et que vous voulez les récupérer chez vous. 

## Envoyer des changements

On utilise `git push` pour envoyer les changements sur framagit. Normalement si vous êtes sur le PC serveur, il n'y aura pas besoin de se connecter. ~~SVP ne récupérez pas mon MDP je le verrais tout de suite, le premier qui le fait je le dégomme~~

## Se connecter au serveur

??? info "Téléchargement des fichiers"
    
    Les fichiers sont disponibles dans la sections [Fichiers](#fichiers) (Sauf Java 8, allez chercher sur Google.)


Voici les étapes pour se connecter au serveur, sur un client non mis en place

- Télécharger Java 8
- Télécharger SKLauncher
- Installer Forge 1.12.2
- Extraire le fichier .zip `Mods+Optifine.zip` dans le dossier mods de minecraft (%appdata%/.minecraft/mods)
- Lancer la version `Forge-1.12.2-xxxx`
- Se connecter au serveur

Sinon vous utilisez l'instance MultiMC comme des chads


## Fichiers

Les fichiers a télécharger sont disponible [ici](https://mega.nz/folder/gBIhSJZb#ixpPd1EjWVPehu1b4-m9MA)

Voici un résumé des fichiers présents :

- Backup-Monde-1.12.2.zip : Monde, version 1.12.2, Pre mods et mise a jour des plugins
- forge-1.12.2-xxx.jar : Installateur de Forge pour Minecraft 1.12.2
- Minecraft-SFX-MultiMC.zip : Instance MultiMC pour se connecter au serveur sans accrocs (Minecraft Premium uniquement), avec quelques extras (JourneyMap + JEI)
- Mods+Optifine.zip : Le modpack de prod, le minimum de mods requis pour se connecter au serveur.
- Old-Server-1.8.9.zip : Le tout premier serveur, Minecraft Vanilla 1.8.9, Spigot
- Server-Magma-1.12.2.zip : Le serveur actuel, sûrement non a jour, priviliger la version git.
- SKlauncher 3.0.exe : Launcher crack, exécutable sur Windows uniquement
- SKlauncher 3.0.jar : Launcher crack, exécutable partout

## Rajouter des mods/plugins

Bon si vous voulez rajouter un plugin/mod, réfléchissez un peu. Est-ce vraiment utile ? Un mod en plus n'est pas trop lourd pour les PCs ? Est-il compatible ? Si la réponse a ces question est oui, Placez les plugins dans le dossier plugins du serveur, et dans le dossier mods pour les mods. N'oubliez pas de redémarer le serveur.

## Faire de chez soi

<!-- TODO: Tutorial -->

Trop long à expliquer par écrit, voici un tuto vidéo : Soon
Cette vidéo est aussi applicable si vous déplacez le serveur sur une autre machine ou un autre OS, mais franchement, qui voudrais quitter Arch ????

## Conclusion

Voila, normalement vous avez tout pour faire tourner le serveur. Si vous voulez plus de détails techniques, rendez vous [ici](../200-Stack). Je vous invite également à explorer ce site, c'est le fruit d'une année d'accumulation de ressources (Traduction, adaptation, synthétisation, mise en forme). De la même façon, n'hésitez pas à contribuer à cette collection. Si vous voulez avoir les droit n'hézitez pas a m'envoyer une demande sur Discord (Xotak#8274) ou de m'envoyer un [mail](mailto:j.l.emeraud@gmail.com?subject=Write%20access%20serveur%20minecraft%20ressources).

\- Votre dévoué serviteur, Jean-Louis, équipe Backstage