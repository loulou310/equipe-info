# Bases du HTML

Ce document traite des bases du HTML.

## Structure de base

La structure d'un fichier HTML est la même dans la plus part des cas. 

Un élément est délimité par des **balises**. La structure des balises est la même tout au long d'un fichier : `<balise>`. Une balise doit être fermée, sinon tous les éléments seront affectés par la balise jusqu'a la fin du fichier, ou la femeture de la balise. On la ferme avec `</balise>`. Dans certains cas, les balises sont seulement ouvrantes.

```html title="Structure de base d'un fichier html"
<!DOCTYPE html> <!-- (1) -->
<html> <!-- (2)  -->
    <head> <!-- (3) -->

    </head>
    <body> <!-- (4) -->

    </body>
</html>
```

1. On définit le type de document : html
2. On ouvre une balise html, tout ce qui sera écrit dans les balises sera de l'HTML
3. On ouvre une balise head. Cette balise contient toutes les informations de la page comme le titre, l'encodage des caractères, les fichiers css ou javascript et d'autres. Ces informations 
4. La balise body contient le corps de la page.

## Balises communes

Traduit et adapté de "HTML quick reference" par l'équipe DuckDuckHack.

### Métadonées du document

`<head>` : Donne des métadonées a propos du document, notamment son titre, des scripts et des feuilles de style.

`<link>` : Spécifie un lien vers une ressource externe, typiquement une feuille de style.

`<meta>` : N'importe quelle information qui ne peut pas être représentée par un autre élément HTML.

`<style>` : Contient des informations de style, comme dans un fichier css. Peu recommandé car cela peut rendre la codebase moins lisible.

`<title>` : Défitit le titre du document, affiché dans la barre de titre du navigateur ou sur l'onglet de la page.

### Sectionnement de contenu

`<body>` : Représente le contenu d'un document. Il ne peut y avoir qu'un `<body>` par document.

`<footer>` : Représente le pied de la page, qui contient typiquement les donées de copyright, et des liens vers les documents en rapport.

`<header>` : Représente un groupe d'élément comme un logo, une barre de recherche et une barre de navigation.

`<h1-6>` : Un élément heading décrit brièvement le sujet de la section qu'il introduit. `<h1>` est le plus important et `<h6>` le moins.

### Contenu de texte

`<div>` : Un conteneur générique pour du contenu. Il peut être utilisé pour regrouper des élément pour les mettre en forme.

`<hr>` : Représente une rupture thématique entre les paragraphes.

`<ol>` : Représente une liste ordonée, typiquement affiché avec un nombrement séquentiel.

`<ul>` : Représente une liste désordonée, typiquement affichée avec des bullet points.

`<li>` : Représente un élément d'une liste. Doit être contenu dans une liste ordonée ou désordonée.

`<p>` : Représente un paragraphe de texte.

### Mise en forme du texte inline

`<a>` : Définit un hyperlien vers un emplacement sur la même page ou n'importe quelle autre page sur internet.

`<span>` : Un conteneur inline pour grouper une partie d'un texte afin de les styliser

`<b>` : Affiche un texte en **Gras**

`<br>` : Représente un saut de ligne dans un texte

`<em>` : Marque du texte qui a une emphase. Utile pour les utilisateurs qu

`<i>` : Représente un texte en *italique*

`<s>` : représente un texte <s>barré</s>

`<strong>` : Donne une grande importance aux yeux d'un lecteur de texte. Souvent affiché en **gras**

`<u>` : affiche le texte en <u>souligné</u>

### Image et multimédia

`<audio>` : Sert a intégrer un audio dans une page

`<img>` : Sert a intéger une image

`<video>` : sert a intégrer une vidéo.

### Contenu intégré

`<iframe>` : Sert a intégrer une autre page web dans une page HTML


Tous les usages de ces balises peuvent être trouvés sur le [MDN](https://developer.mozilla.org/en-US/docs/Web/HTML)