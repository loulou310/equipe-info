# Rust - Commentaires

Tous les programmeurs aspirent a rendre leur code compréhensibles, mais parfois, plus d'explications sont nécessaires. Dans ces cas la, les programmeurs laissent des commentaires dans leur code que le compileur ignore mais que les lecteurs trouverons certainement utiles.

Voici un commentaire simple : 

```rs
// hello, world
```

En Rust, le style des commentaires débute un commentaire avec deux slashs, et le commentaire continue jusqu'a la fin de la ligne. Pour les commentaires qui s'étendent sur plusieurs lignes, on va inclure un double-slash sur chaque ligne comme ceci : 

```rs
// Donc ici on fait un truc très compliqué, tellement long qu'on a besoin de
// plusieurs lignes de commentaires pour l'expliquer. J'espère que ce commentaire va 
// expliquer ce qu'il se passe
```

Les commentaires peuvent également être placés a la fin de ligne de code :

```rs
fn main() {
    let nombre_chance = 7; // J'ai de la chance aujourd'hui
}
```
Mais on trouvra plus souvent des commentaires dans ce format, avec le commentaire dans une ligne séparée au dessus du code qu'il annote : 

```rs
fn main() {
    // I’m feeling lucky today
    let lucky_number = 7;
}
```
