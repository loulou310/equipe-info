# Arch Linux

![Logo de Arch Linux](../../pictures/Arch-logo.png){ align=left }
Arch Linux est une distribution Linux créée par Judd Vinet qui met l'accent sur la simplicité (selon le principe KISS). Judd a été inspiré par une autre distribution Linux appelée Crux Linux. Arch Linux a été conçue comme un système d'exploitation destiné aux utilisateurs avancés. Sa philosophie simple et sans outils de configuration demande, comme Slackware, quelques habitudes de Linux pour être installée, mais reste toutefois simple à maintenir.

<br>
<br>

## Gestionaire de paquets

Le gestionnaire de paquets s'appelle pacman (package management utility). Il peut gérer la mise à jour du système, l'installation de nouveaux paquets, la suppression de paquets installés, et différentes recherches. 

L'installation des paquets passe par ABS : Arch Linux Build System, un système ressemblant aux « ports » des BSD. ABS permet d'installer des paquets binaires précompilés, ou de compiler un paquet depuis ses sources. Les paquets binaires, les plus courants, sont gérés par le gestionnaire de paquets pacman. Les paquets sont optimisés pour les architectures i686 et x86 64, et les paquets des dépôts officiels sont maintenus par des Trusted Users, des utilisateurs de confiance.

### Utilisation
- `pacman -S paquet` : Installe paquet
- `pacman -Sy` : Met les bases de données des paquets à jour. Nécessaire au premier démarrage ou après un certain temps sans mise a jour
- `pacman -Syu` : Met les bases de données à jour et démarre la mise a jour complette du système
- `pacman -R paquet` : Désinstalle paquet, sert également a désinstaller les paquets provenant du AUR

## AUR

Des paquets supplémentaires peuvent être installés depuis le AUR (**A**rch **U**ser **R**epository). C'est une collection de fichiers PKGBUILD, qui contiennent une liste d'instruction pour la récupération des sources, la compilation, l'installation et le paramétrage des paquets.

Des paquets sont ajoutés dans le AUR pour plusieurs raisons : 
- Problèmes de licensement : Des logiciels qui ne peuvent pas être distribués mais qui sont gratuits d'utilisations comme des logiciels propriétaires tel que Google Earth
- Des paquets officiels modifiés
- Des logiciels peu populaires
- Des versions beta de logiciels

Une interface utilisable est [yay](https://github.com/Jguer/yay)

### Utilisation

- `yay` : même chose que `pacman -Syu` et fait ensuite la mise a jour des paquets installés avec yay
- `yay -S paquet` : installe paquet