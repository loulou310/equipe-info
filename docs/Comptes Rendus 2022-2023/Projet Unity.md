# PROJET UNITY 2022-2023
## Plan :
    Ce que l'on a fait;
    La version du projet;
    Les assets qui nous ont servi;
    Les problèmes rencontrés;
    Ce que les joueurs en ont pensé;
### Nous avons créé un projet via l'application de création de jeu vidéal 2D-3D Unity ver.2021.3.20.f1 (Hub 3.4.1)
#### Nous avons utilisé l"asset store pour télécharger le 3D games kits pour nous aider à concevoir le projet Unity.
##### Nous avons eu des problèmes, comme par exemple : nous être trompés de fichier et avoir pris dans le dossier "art" plutôt que "préfabs", la fonction "layer" qu'il ne faut pas oublier de changer, sinon ça marche pas, le clavier anglais qu'il faut activer pour avoir accès aux commandes de déplacement de base "ZQSD" mais qui change le "ctrl z" en "ctrl w" ce qui ferme le projet (d'ailleurs le **bouton au-dessus du bouton éteindre, éteint également le PC**), oublier d'enlever les "components" inutiles, oublier de  sortir du jeu avant de faire des modifications. On a eu des problèmes avec le "nav mesh surface" car si tu ne modifies pas, les mobs ne pourront pas marcher dessus.
###### Les joueurs qui ont testé notre jeu ont tous aimé et ont (notamment Sacha Boulet-Beyer) même eu l'idée de le speedrun, ils ont dit que ce jeu avait même beaucoup d'avenir sur "Steam" (bon, je pense personnellement que si on le met sur "Steam", on devrait remplacer toutes les textures par des textures originales) et qu'en open world, ça serait mieux, et une difficulté à la "Dark Souls" mais aussi qu'il est un peu court.
Conclusion :
Nous avons très mal commencé le projet sans assets ce qui a fait que nous avons dû recommencer un grand nombre de fois avant de prendre le 3D Game kit et de pouvoir commencer une fois que nous avons compris qu'il ne fallait pas prendre dans "arts" mais dans "préfabs" ensuite une fois que nous avons pu commencer réellement à travailler pour construire le jeu donc je recommande de directement commencer avec le 3D game kit pour pouvoir faire quelque chose de mieux et mieux comprendre comment marche Unity.