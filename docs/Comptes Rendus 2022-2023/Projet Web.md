# Projet web

## Responsable du projet

Jean-Louis

## Membres du projet 

- Nolann
- Benjamin
- Théo
- Nathanaël
- Oscar

## But du projet

Effectuer une initiation au développement web, en poussant parfois les choses un peu plus loin.

## Technologies utilisées : 

HTML, CSS, Bootstrap (Evoqué rapidement).