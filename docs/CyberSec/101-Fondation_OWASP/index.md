# Fondation OWASP

Le *Open Web Application Security Project* (OWASP) est une organisation a but non lucratif qui œuvre pour améliorer la sécurité des logiciels. Grâce à des projets de logiciels libres dirigés par la communauté, des centaines de chapitres locaux dans le monde entier, des dizaines de milliers de membres et des conférences d'éducation et de formation de premier plan, la Fondation OWASP est la source pour les développeurs et les technologues pour sécuriser le web.

Elle met a disposition :

- Outils et ressources
- Communauté et réseau
- Education et entrainement

## Projets notables

Cette section liste les projets les plus connus de la fondation.

### OWASP top 10

Le Top 10 de l'OWASP est un livre/document de référence qui présente les 10 problèmes de sécurité les plus critiques pour la sécurité des applications web. Le rapport est rédigé par une équipe d'experts en sécurité du monde entier. Les données proviennent d'un certain nombre d'organisations et sont ensuite analysées.

Voici le top 10 de 2021

- Controle d'accès défaillant
- Défaillances cryptographiques
- Injection
- Conception non sécurisée
- Mauvaise configuration de sécurité
- Composants vulnérables ou obsolètes
- Identification et authentification de mauvaise qualité 
- Manque d'intégrité des données et du logiciel
- Carence des systèmes de contrôle et de journalisation 
- Falsification de requête côté serveur (SSRF)

Plus d'informations : [https://owasp.org/Top10/fr/](https://owasp.org/Top10/fr/)


### Juice Shop

!!! Quote "Description"

    OWASP Juice Shop est probablement l'application web non sécurisée la plus moderne et la plus sophistiquée ! Elle peut être utilisée dans les formations à la sécurité, les démonstrations de sensibilisation, les CTF et comme cobaye pour les outils de sécurité ! Juice Shop englobe les vulnérabilités de l'ensemble du Top Ten de l'OWASP ainsi que de nombreuses autres failles de sécurité trouvées dans des applications du monde réel !


Le Juice Shop est un outil open-source prennant la forme d'un site d'e-commerce de vente de jus de fruits. Cela permet d'essayer des outils ou tout simplement de s'entrainer. Une démo est accessible [ici](https://juice-shop.herokuapp.com/)

![|](../../pictures/SS-juice-shop.png)